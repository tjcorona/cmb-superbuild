superbuild_set_revision(boost
  URL     "https://midas3.kitware.com/midas/download/bitstream/457867/boost_1_60_0.tar.bz2"
  URL_MD5 65a840e1a0b13a558ff19eeb2c4f0cbe)

# XXX: When updating this, update the version number in CMakeLists.txt as well.
# Using  ParaView 5.1.2
#
# Currently using a snapshot of ParaView that is based on 5.1.2 but has no
# version of its own, so I'm leaving the CMakeLists.txt variable as is
set(paraview_revision 602bc96d956767617f133bb63554690bf0cb227c)
if (USE_PARAVIEW_master)
  set(paraview_revision origin/master)
endif ()
superbuild_set_revision(paraview
  GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
  GIT_TAG        "${paraview_revision}")

superbuild_set_revision(shiboken
  GIT_REPOSITORY "https://github.com/OpenGeoscience/shiboken.git"
  GIT_TAG        origin/smtk-head)

superbuild_set_external_source(smtk
  "https://gitlab.kitware.com/cmb/smtk.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_external_source(cmb
  "https://gitlab.kitware.com/cmb/cmb.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG        origin/master)

superbuild_set_revision(vxl
  URL     "https://github.com/judajake/vxl/archive/44433e4bd8ca3eabe4e5441444bf2a050d689d45.tar.gz"
  URL_MD5 6ca7a87bebb12338a45277b921af4277)

superbuild_set_revision(opencv
  GIT_REPOSITORY "https://github.com/opencv/opencv.git"
  GIT_TAG 74e997f15b20600895896e1a837a0bb3cbf7f07b ) #Master at Aug 11 2016

superbuild_set_revision(opencvcontrib
  GIT_REPOSITORY "https://github.com/opencv/opencv_contrib.git"
  GIT_TAG 73459049a323af2c88e89037b45fadb7085020fb ) #Master at Aug 11 2016

# Use the tweaked cmake build of zeromq
superbuild_set_revision(zeromq
  GIT_REPOSITORY "https://github.com/robertmaynard/zeromq4-x.git"
  GIT_TAG        origin/master)

# Use remus from Fri Jul 29 15:25:33 2016 -0400
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        378c70d5f7ebd3323e724f60b4dbc2ba2dfc3011)

superbuild_set_revision(kml
  URL     "http://vtk.org/files/support/libkml_fa6c7d8.tar.gz"
  URL_MD5 261b39166b18c2691212ce3495be4e9c)

superbuild_set_revision(gdal
  GIT_REPOSITORY "https://github.com/judajake/gdal-svn.git"
  GIT_TAG        origin/gdal-1.11-cmb)

superbuild_set_revision(moab
  GIT_REPOSITORY "https://bitbucket.org/mathstuf/moab.git"
  GIT_TAG        origin/next)

superbuild_set_revision(triangle
  GIT_REPOSITORY "https://github.com/robertmaynard/triangle.git"
  GIT_TAG        origin/master)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsetuptools
  URL     "https://pypi.python.org/packages/45/5e/79ca67a0d6f2f42bfdd9e467ef97398d6ad87ee2fa9c8cdf7caf3ddcab1e/setuptools-23.0.0.tar.gz"
  URL_MD5 100a90664040f8ff232fbac02a4c5652)

superbuild_set_revision(pythongirderclient
  URL     "https://pypi.python.org/packages/source/g/girder-client/girder-client-1.1.2.tar.gz"
  URL_MD5 4cd5e0cab41337a41f45453d25193dcf)

superbuild_set_revision(ftgl
  GIT_REPOSITORY "https://github.com/mathstuf/ftgl.git"
  GIT_TAG        origin/create-cmake-config-file)

superbuild_set_revision(oce
  GIT_REPOSITORY "https://github.com/mathstuf/oce.git"
  GIT_TAG        origin/next-x11)

superbuild_set_revision(cgm
  GIT_REPOSITORY "https://bitbucket.org/mathstuf/cgm.git"
  GIT_TAG        origin/update-cmakelists)
