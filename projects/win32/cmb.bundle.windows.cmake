set(CPACK_MONOLITHIC_INSTALL TRUE)

# URL to website providing assistance in installing your application.
set(CPACK_NSIS_HELP_LINK "https://gitlab.kitware.com/cmb/cmb/wikis/home")

#FIXME: need a pretty icon.
#set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_LIST_DIR}/paraview.ico")

set(ModelBuilder_description "Model Builder")
set(paraview_description "ParaView")
set(pvserver_description "ParaView (server)")
set(pvdataserver_description "ParaView (data server)")
set(pvrenderserver_description "ParaView (render server)")
set(pvpython_description "ParaView (Python shell)")

set(library_paths "lib")

if (USE_SYSTEM_qt4)
  list(APPEND library_paths
    "${QT_LIBRARY_DIR}")
endif ()

set(plugins)
foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  if (DEFINED "${executable}_description")
    list(APPEND CPACK_NSIS_MENU_LINKS
      "bin/${executable}.exe" "${${executable}_description}")
  else ()
    message(WARNING "No description for ${executable} given.")
  endif ()

  superbuild_windows_install_program("${executable}"
    "${library_paths}")
  list(APPEND plugins
    ${cmb_plugins_${executable}})
endforeach ()

list(REMOVE_DUPLICATES plugins)

foreach (plugin IN LISTS plugins)
  superbuild_windows_install_plugin("${plugin}.dll"
    "bin"
    "${library_paths}")
endforeach ()

superbuild_windows_install_python(
  "${CMAKE_INSTALL_PREFIX}"
  MODULES ${cmb_python_modules}
  MODULE_DIRECTORIES
          "${superbuild_install_location}/bin/Lib/site-packages"
          "${superbuild_install_location}/lib/site-packages"
          "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES
          "lib")

superbuild_windows_install_python(
  "${CMAKE_INSTALL_PREFIX}"
  MODULES vtk
  NAMESPACE paraview
  MODULE_DIRECTORIES
          "${superbuild_install_location}/bin/Lib/site-packages"
          "${superbuild_install_location}/lib/site-packages"
          "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES
          "lib")

include(python.functions)
superbuild_install_superbuild_python()

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/bin/Lib/site-packages/requests/cacert.pem"
    DESTINATION "bin/Lib/site-packages/requests"
    COMPONENT   superbuild)
endif ()

set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/.plugins")
cmb_add_plugin("${plugins_file}" ${plugins})

install(
  FILES       "${plugins_file}"
  DESTINATION "bin"
  COMPONENT   superbuild)

install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "share/cmb/workflows"
  COMPONENT   superbuild)
