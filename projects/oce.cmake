superbuild_add_project(oce
  DEPENDS ftgl freetype
  DEPENDS_OPTIONAL cxx11
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DOCE_DISABLE_X11:BOOL=ON
    -DOCE_INSTALL_PREFIX:FILEPATH=<INSTALL_DIR>
    # Force the include dir path, so it doesn't install the include files into
    # install/include/oce/, because CGM can't find it there.
    -DOCE_INSTALL_INCLUDE_DIR:FILEPATH=include
    -DOCE_RPATH_FILTER_SYSTEM_PATHS:BOOL=OFF)

# Remove the installed oce-config.h from the install tree so that the build
# doesn't use that over the configured header in the build directory. If we
# used the installed oce-config the incremental builds may fail as it may not
# have all the defines as the build version.
superbuild_project_add_step(oce-incremental-build
  COMMAND   "${CMAKE_COMMAND}"
            -E remove
            -f
            "<INSTALL_DIR>/include/oce-config.h"
  COMMENT   "Remove header which confuses the build"
  DEPENDEES configure
  DEPENDERS build
  ALWAYS    1)
