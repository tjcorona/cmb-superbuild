superbuild_add_project(opencv
  DEPENDS
    python qt4 opencvcontrib numpy
  CMAKE_ARGS
    -DOPENCV_EXTRA_MODULES_PATH:PATH=${CMAKE_CURRENT_BINARY_DIR}/opencvcontrib/src/modules
    -DBUILD_opencv_hdf:BOOL=OFF
    -DBUILD_opencv_dnn:BOOL=OFF
    -DWITH_CUDA:BOOL=OFF
    -DWITH_VTK:BOOL=OFF
    -DOpenCV_INSTALL_BINARIES_PREFIX:STRING=
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib)
