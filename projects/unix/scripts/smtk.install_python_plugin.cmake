include(GNUInstallDirs)

set(SHARED_LIBRARY_SUFFIX "so")
set(PYTHON_SITE "lib/python2.7/site-packages")
set(PYTHON_INSTALL "lib/paraview-${paraview_version}/site-packages")
set(LIB_SHIBOKEN_PY "lib/libshiboken-python2.7.so.1.2")
if (NOT EXISTS ${smtk_bin_dir}/${LIB_SHIBOKEN_PY})
  set(LIB_SHIBOKEN_PY "lib/libshiboken-python2.7-dbg.so.1.2")
  if (NOT EXISTS ${smtk_bin_dir}/${LIB_SHIBOKEN_PY})
    message(SEND_ERROR "could not create smtk python package.  libshiboken is missing")
    return ()
  endif ()
endif ()

file(REMOVE_RECURSE "${tmp_dir}/SMTKPlugin")
file(MAKE_DIRECTORY "${tmp_dir}/SMTKPlugin")

set(plugin_install_dir ${smtk_bin_dir}/${PYTHON_INSTALL}/smtk)
file(MAKE_DIRECTORY "${plugin_install_dir}")

set(PYTHON_COPY_SOURCE_LOC lib/python2.7/site-packages)

set(py_files_libs ${smtk_bin_dir}/${PYTHON_SITE}/smtk/smtkCorePython.${SHARED_LIBRARY_SUFFIX}
                  ${smtk_bin_dir}/${LIB_SHIBOKEN_PY}
                  ${smtk_bin_dir}/${PYTHON_SITE}/shiboken.${SHARED_LIBRARY_SUFFIX}
                  ${smtk_bin_dir}/${PYTHON_COPY_SOURCE_LOC}/smtk/simple.py)
foreach (lib IN LISTS py_files_libs)
  configure_file("${lib}" "${tmp_dir}/SMTKPlugin" COPYONLY)
endforeach ()

#okay the plugin is fixed up, now we need to install it into paraviews bundle
file(GLOB fixedUpLibs "${tmp_dir}/SMTKPlugin/*")
foreach (lib IN LISTS fixedUpLibs)
  configure_file("${lib}" "${plugin_install_dir}" COPYONLY)
endforeach ()

# For linux, also copy smtk libs where standalone export app can find them
set(STANDALONE_EXPORT_DIR ${smtk_bin_dir}/${PYTHON_SITE}/smtk)
file(MAKE_DIRECTORY "${STANDALONE_EXPORT_DIR}")
foreach (lib IN LISTS fixedUpLibs)
  configure_file("${lib}" "${STANDALONE_EXPORT_DIR}" COPYONLY)
endforeach ()
