set(library_paths
  "${superbuild_install_location}/lib"
  "${superbuild_install_location}/lib/paraview-${paraview_version}"
  "${superbuild_install_location}/lib/cmb-${cmb_version}")

if (QT_LIBRARY_DIR)
  list(APPEND library_paths
    "${QT_LIBRARY_DIR}")
endif ()

set(plugins)
foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  superbuild_unix_install_program_fwd("${executable}"
    "lib/paraview-${paraview_version};lib/cmb-${cmb_version}"
    SEARCH_DIRECTORIES "${library_paths}")
  list(APPEND plugins
    ${cmb_plugins_${executable}})
endforeach ()

list(REMOVE_DUPLICATES plugins)

foreach (plugin IN LISTS plugins)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "lib/cmb-${cmb_version}"
    "lib;lib/cmb-${cmb_version}"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/cmb-${cmb_version}/plugins/${plugin}/")
endforeach ()

superbuild_unix_install_python(
  LIBDIR              "lib/cmb-${cmb_version}"
  MODULES             ${cmb_python_modules}
  MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python2.7/site-packages"
                      "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES  "${library_paths}")

superbuild_unix_install_python(
  MODULE_DESTINATION  "/site-packages/paraview"
  LIBDIR              "lib/cmb-${cmb_version}"
  MODULES             vtk
  MODULE_DIRECTORIES "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES "${library_paths}")

if (cmb_install_paraview_python)
  superbuild_unix_install_python(
    LIBDIR              "lib/paraview-${paraview_version}"
    MODULES             paraview vtk
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python2.7/site-packages"
                        "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES  "${library_paths}")

  superbuild_unix_install_python(
    MODULE_DESTINATION  "/site-packages/paraview"
    LIBDIR              "lib/paraview-${paraview_version}"
    MODULES             vtk
    MODULE_DIRECTORIES "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES "${library_paths}")
endif ()

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/python2.7/site-packages/requests/cacert.pem"
    DESTINATION "lib/python2.7/site-packages/requests"
    COMPONENT   superbuild)
endif ()

include(python.functions)
superbuild_install_superbuild_python()

set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/.plugins")
cmb_add_plugin("${plugins_file}" ${plugins})

install(
  FILES       "${plugins_file}"
  DESTINATION "lib/cmb-${cmb_version}"
  COMPONENT   superbuild)

install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "share/cmb/workflows"
  COMPONENT   superbuild)

# ParaView expects this directory to exist.
install(CODE
  "file(MAKE_DIRECTORY \"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/lib/paraview-${paraview_version}\")"
  COMPONENT   superbuild)
