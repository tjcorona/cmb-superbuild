set(paraview_extra_cmake_args)
if (PV_NIGHTLY_SUFFIX)
  list(APPEND paraview_extra_cmake_args
    -DPV_NIGHTLY_SUFFIX:STRING=${PV_NIGHTLY_SUFFIX})
endif ()

set(paraview_disable_plugins
  EyeDomeLighting
  SciberQuestToolKit
  PointSprite
  NonOrthogonalSource
  PacMan
  StreamingParticles
  SierraPlotTools
  SLACTools
  UncertaintyRendering
  SurfaceLIC
  EyeDomeLighting
  RGBZView
  MobileRemoteControl)
foreach (plugin IN LISTS paraview_disable_plugins)
  list(APPEND paraview_extra_cmake_args
    -DPARAVIEW_BUILD_PLUGIN_${plugin}:BOOL=FALSE)
endforeach ()

if (APPLE)
  list(APPEND paraview_extra_cmake_args
    # We are having issues building mpi4py with Python 2.6 on Mac OSX. Hence,
    # disable it for now.
    -DPARAVIEW_USE_SYSTEM_MPI4PY:BOOL=ON
    # We set the VTK_REQUIRED_OBJCXX_FLAGS to nothing to work around the fact
    # that the current version of vtk tries to enable objc garbage collection
    # which has been removed in XCode 5
    -DVTK_REQUIRED_OBJCXX_FLAGS:STRING=)
endif ()

if (UNIX AND NOT APPLE)
  list (APPEND paraview_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

if (__BUILDBOT_INSTALL_LOCATION)
  list(APPEND paraview_extra_cmake_args
    -DPARAVIEW_CUSTOM_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>
    -DPARAVIEW_DO_UNIX_STYLE_INSTALLS:BOOL=ON)
endif ()

list(APPEND paraview_extra_cmake_args
  -DPARAVIEW_EXTRA_EXTERNAL_PLUGINS:STRING=CMB_Plugin)

#this can't be quoted, since that will introduce an extra
#set of quotes into pqparaviewInitializer, and break the build
set(paraview_optional_plugins CMB_Plugin${_superbuild_list_separator}KMLExporter_Plugin)

superbuild_add_project(paraview
  DEBUGGABLE
  DEPENDS
    boost
    gdal
    png
    python
    qt4
    zlib
    netcdf
  DEPENDS_OPTIONAL
    cxx11 freetype hdf5
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DPARAVIEW_BUILD_PLUGIN_CoProcessingScriptGenerator:BOOL=ON
    -DPARAVIEW_BUILD_QT_GUI:BOOL=${qt4_enabled}
    -DPARAVIEW_ENABLE_PYTHON:BOOL=${python_enabled}
    -DPARAVIEW_ENABLE_WEB:BOOL=OFF
    -DPARAVIEW_USE_MPI:BOOL=${mpi_enabled}
    -DVTK_USE_SYSTEM_HDF5:BOOL=${hdf5_enabled}
    -DVTK_USE_SYSTEM_NETCDF:BOOL=${netcdf_enabled}
    -DVTK_RENDERING_BACKEND:STRING=OpenGL2

    #currently catalyst is having problems on praxis so lets disable it for now
    -DPARAVIEW_ENABLE_CATALYST:BOOL=OFF

    #CMB needs geovis enabled to provide the kml and gdal readers
    -DModule_vtkGeovisCore:BOOL=ON
    -DModule_vtkIOGDAL:BOOL=ON
    -DModule_vtkViewsInfovis:BOOL=ON
    -DModule_vtkRenderingMatplotlib:BOOL=ON
    -DModule_vtkRenderingGL2PSOpenGL2:BOOL=ON
    -DGDAL_DIR:PATH=<INSTALL_DIR>

    # CMB needs to specify external plugins so that we can let paraview
    # properly install the plugins. So we sneakily force a variable that is an
    # implementation detail of paraview branding
    -DBPC_OPTIONAL_PLUGINS:INTERNAL=${paraview_optional_plugins}
    -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=TRUE
    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_USE_SYSTEM_FREETYPE:BOOL=${freetype_enabled}
#    currently png strips rpaths don't use this in cmb, so don't use this
#    -DVTK_USE_SYSTEM_PNG:BOOL=${png_enabled}
    -DVTK_USE_SYSTEM_ZLIB:BOOL=${zlib_enabled}

    # Specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    #If this is true paraview doesn't properly clean the paths to system
    #libraries like netcdf
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=TRUE

    ${paraview_extra_cmake_args})
