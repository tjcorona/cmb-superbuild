set(paraview_dir ${CMAKE_CURRENT_BINARY_DIR}/paraview/build)

if (__BUILDBOT_INSTALL_LOCATION)
  set(paraview_dir <INSTALL_DIR>/lib/cmake/paraview-${paraview_version})
endif ()

set(smtk_extra_cmake_args)
if (WIN32)
  # On Windows we expect the Python source for module to be
  # in a different place than Unix builds and in a different
  # place than SMTK would put it by default. Tell SMTK where
  # to install Python source for the smtk module:
  list(APPEND smtk_extra_cmake_args
    "-DSMTK_PYTHON_MODULEDIR:PATH=bin/Lib/site-packages")
endif ()

set(smtk_libdir lib)
if (WIN32)
  set(smtk_libdir bin)
endif ()

set(SMTK_DATA_DIR "" CACHE PATH "Location of SMTK's testing data")

#explicitly depend on gdal so we inherit the location of the GDAL library
#which FindGDAL.cmake fails to find, even when given GDAL_DIR.
superbuild_add_project(smtk
  DEVELOPER_MODE
  DEBUGGABLE
  DEPENDS boost cxx11 qt4 shiboken paraview python remus hdf5 zeromq gdal
  DEPENDS_OPTIONAL moab netcdf cgm
  CMAKE_ARGS
    ${smtk_extra_cmake_args}
    -DBUILD_SHARED_LIBS:BOOL=ON

    -DSMTK_ENABLE_QT_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_VTK_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_PARAVIEW_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_CGM_SESSION:BOOL=${cgm_enabled}
    -DSMTK_ENABLE_DISCRETE_SESSION:BOOL=ON
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMOTE_SESSION:BOOL=ON
    -DSMTK_ENABLE_REMUS_SUPPORT:BOOL=ON
    -DSMTK_ENABLE_PYTHON_WRAPPING:BOOL=${shiboken_enabled}
    -DCMAKE_INSTALL_LIBDIR:STRING=${smtk_libdir}
    -DSMTK_DATA_DIR:PATH=${SMTK_DATA_DIR}

    # This should be off by default because vtkCmbMoabReader in discrete
    # session may only be needed for debugging purpose
    -DSMTK_ENABLE_MOAB_DISCRETE_READER:BOOL=OFF

    -DSMTK_USE_SYSTEM_MOAB:BOOL=${moab_enabled}
    -DMOAB_INCLUDE_DIR:PATH=<INSTALL_DIR>/include

    # MOAB bits
    -DENABLE_HDF5:BOOL=${hdf5_enabled}
    -DENABLE_NETCDF:BOOL=${netcdf_enabled}
    -DNETCDF_ROOT:PATH=<INSTALL_DIR>

    -DParaView_DIR:PATH=${paraview_dir}
    # GDAL bits to properly enable gdal classes ( mainly wrapper )
    # that we need to build
    -DGDAL_DIR:PATH=<INSTALL_DIR>

    -DSMTK_FIND_SHIBOKEN:STRING=)

if (WIN32)
  set(smtk_cmakedir bin/cmake)
else ()
  set(smtk_cmakedir lib/cmake)
endif ()

superbuild_add_extra_cmake_args(
  -DSMTK_DIR:PATH=<INSTALL_DIR>/${smtk_cmakedir}/SMTK)
