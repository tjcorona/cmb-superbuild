# Consolidates platform independent stub for cmb.bundle.cmake files.

include(cmb-version)
include(paraview-version)

set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_RESOURCE_FILE_LICENSE
    "${superbuild_source_directory}/License.txt")
set(CPACK_PACKAGE_VERSION_MAJOR ${cmb_version_major})
set(CPACK_PACKAGE_VERSION_MINOR ${cmb_version_minor})
if (cmb_version_suffix)
  set(CPACK_PACKAGE_VERSION_PATCH ${cmb_version_patch}-${cmb_version_suffix})
else()
  set(CPACK_PACKAGE_VERSION_PATCH ${cmb_version_patch})
endif()

SET(CPACK_PACKAGE_INSTALL_DIRECTORY
  "${cmb_package_name} ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME
  "${cmb_package_name}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")

if(NOT DEFINED CPACK_SYSTEM_NAME)
  set(CPACK_SYSTEM_NAME ${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR})
endif()
if(${CPACK_SYSTEM_NAME} MATCHES Windows)
  if(CMAKE_CL_64)
    set(CPACK_SYSTEM_NAME win64)
  else(CMAKE_CL_64)
    set(CPACK_SYSTEM_NAME win32)
  endif()
endif()
if(NOT DEFINED CPACK_PACKAGE_FILE_NAME)
  set(CPACK_PACKAGE_FILE_NAME
    "${CPACK_SOURCE_PACKAGE_FILE_NAME}-${CPACK_SYSTEM_NAME}")
endif()

list(SORT cmb_programs_to_install)
list(REMOVE_DUPLICATES cmb_programs_to_install)

function (cmb_add_plugin output)
  set(contents "<?xml version=\"1.0\"?>\n<Plugins>\n</Plugins>\n")
  foreach (name IN LISTS ARGN)
    set(plugin_directive "  <Plugin name=\"${name}\" auto_load=\"1\" />\n")
    string(REPLACE "</Plugins>" "${plugin_directive}</Plugins>" contents "${contents}")
  endforeach ()
  file(WRITE "${output}" "${contents}")
endfunction ()

set(paraview_executables)
if (cmb_install_paraview_server)
  list(APPEND paraview_executables
    pvserver
    pvdataserver
    pvrenderserver)
endif ()
if (cmb_install_paraview_python)
  list(APPEND paraview_executables
    pvbatch
    pvpython)
endif ()

set(cmb_plugins_smtk
  smtkDiscreteSessionPlugin
  smtkExodusSessionPlugin
  smtkPolygonSessionPlugin
  smtkRemoteSessionPlugin
  smtkRemusMeshOperatorPlugin)

if (cgm_enabled)
  list(APPEND cmb_plugins_smtk
    smtkCGMSessionPlugin)
endif ()

set(cmb_plugins_cmb
  CMB_Plugin
  ModelBridge_Plugin)

set(cmb_plugins_all
  ${cmb_plugins_smtk}
  ${cmb_plugins_cmb})

set(cmb_plugins_ModelBuilder
  #${cmb_plugins_cmb} # Autoloaded
  ${cmb_plugins_smtk})
set(cmb_plugins_paraview
  CMB_Plugin)

set(cmb_python_modules
  smtk
  shiboken
  paraview
  pygments
  six
  vtk)

if (numpy_enabled)
  list(APPEND cmb_python_modules
    numpy)
endif ()

if (opencv_enabled)
  list(APPEND cmb_python_modules
    cv2)
endif ()

if (pythongirderclient_enabled)
  list(APPEND cmb_python_modules
    requests
    girder_client)
endif ()
